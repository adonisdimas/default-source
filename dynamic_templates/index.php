<?php 
	include('includes/loader.php');
	include(TEMPLATES.'/head.tpl.php');
?>
</head>
<?php include(TEMPLATES.'/blocks/offer-modal.tpl.php');?>
<body>
	<header>
		<?php include(TEMPLATES.'/header.tpl.php');?>
	</header>
	<main>
		<div class="row slider-wrapper">
			<?php include('../'.TEMPLATES.'/blocks/page-slider.tpl.php');?>			
		</div>	
		<div class="row top-bottom-padding">
			<div class="content-padding content-width content-padding">
				<div class="row">
					<div class="col-md-12"><h1><?php echo $translated_page_data['title']; ?></h1></div>
					<div class="col-md-12 top-bottom-padding about-bg">
						<div class="col-md-6 box-content welcome-bg">			
							<?php echo $translated_page_data['content_1']; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
	<footer>
		<?php include(TEMPLATES.'/footer.tpl.php');?>
	</footer>
	<?php include(TEMPLATES.'/scripts.tpl.php');?>
</body>
</html>