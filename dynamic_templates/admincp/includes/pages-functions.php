<?php
	define('GALLERY_DIR', "../assets/img/gallery");  /*define Gallery images directory*/
	$db_object=db_connect();
	if(isset($_GET['lang'])&& isset($_GET['page'])){
		$language=$_GET['lang'];
		$page_id=$_GET['page'];
	}else{
		$language='el';
		$page_id=1;
	}
	$menu_titles=db_get_translated_menu_items($db_object,$language);
	$page_row_data = db_get_translated_page($db_object,$page_id,$language);
	$page_data=db_get_page($db_object,$page_id);
	$page_slider_dir='../assets/img'.$page_data['slider_folder_path'];
	$page_gallery_dir='../assets/img'.$page_data['gallery_folder_path'];
	/*******************UPDATE PAGE ROW**********************************************************/
	$editFormAction = $_SERVER['PHP_SELF'];
	if (isset($_SERVER['QUERY_STRING'])) {
		$editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
	}
	if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form2")) {
		db_update_translated_page($db_object,$page_id,$language,$_POST);
		$updateGoTo = "pages.php?lang=$language&page=$page_id";
		header(sprintf("Location: %s", $updateGoTo));
	}
	/*****************************************************************************/
?>