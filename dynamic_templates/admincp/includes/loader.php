<?php
	ob_start();
	session_start();
	error_reporting(E_ALL);
	ini_set('display_errors', 0);
	$url = $_SERVER['REQUEST_URI']; //returns the current URL
	$parts = explode('/',$url);
	define('ROOT', "http://$_SERVER[HTTP_HOST]/$parts[1]/");  /*ROOT*/
	define('ASSETS', 'assets');
	define('TEMPLATES', 'templates');
	require_once  '../includes/medoo.php';
	require_once  '../includes/functions.php';
	require_once  '../includes/db-functions.php';
	require_once  'includes/session-login.php';
	require_once  'includes/strings.php';
?>