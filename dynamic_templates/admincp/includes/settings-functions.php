<?php
	$db_object=db_connect();
	if(isset($_GET['lang'])){
		$language=$_GET['lang'];
	}else{
		$language='el';
	}
	$page_teasers_row_data = db_get_settings($db_object,1,$language);
	$room_teasers_row_data = db_get_settings($db_object,2,$language);
	$social_row_data = db_get_settings($db_object,3,$language);
	$strings_row_data = db_get_translated_strings($db_object,$language);
	$offers_row_data = db_get_translated_offer($db_object,$language);
	$editFormAction = $_SERVER['PHP_SELF'];
	if (isset($_SERVER['QUERY_STRING'])) {
		$editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
	}
	if (isset($_POST["MM_update"])) {
		if($_POST["MM_update"] == "form2"){
			foreach ($_POST['page_teaser'] as $params){
				db_update_setting($db_object,$params['id'],$params['link'],$params['image_path']);
				db_update_translated_setting($db_object,$language,$params['id'],$params['title'],$params['content_1']);
			}
			$updateGoTo = "settings.php?lang=$language";
			header(sprintf("Location: %s", $updateGoTo));
		}
		if($_POST["MM_update"] == "form3"){
			foreach ($_POST['room_teaser'] as $params){
				db_update_setting($db_object,$params['id'],$params['link'],$params['image_path']);
				db_update_translated_setting($db_object,$language,$params['id'],$params['title'],$params['content_1']);
			}
			$updateGoTo = "settings.php?lang=$language";
			header(sprintf("Location: %s", $updateGoTo));
		}
		if($_POST["MM_update"] == "form4"){
			foreach ($_POST['social'] as $params){
				db_update_setting($db_object,$params['id'],$params['link'],$params['image_path']);
			}
			$updateGoTo = "settings.php?lang=$language";
			header(sprintf("Location: %s", $updateGoTo));
		}
		if($_POST["MM_update"] == "form5"){
			foreach ($_POST['strings'] as $params){
				db_update_translated_string($db_object,$params['id'],$params['value']);
			}
			$updateGoTo = "settings.php?lang=$language";
			header(sprintf("Location: %s", $updateGoTo));
		}
		if($_POST["MM_update"] == "form6"){
			if(isset($_POST['offers']['flag'])){
				$flag=1;
			}else{
				$flag=0;
			}
			db_update_translated_offer($db_object,$_POST['offers'],$flag);
			$updateGoTo = "settings.php?lang=$language";
			header(sprintf("Location: %s", $updateGoTo));
		}
	}
	/*****************************************************************************/
?>