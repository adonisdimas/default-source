<?php
	define('ROOM_DEFAULT_DIR', "../img/rooms/default");  /*define room images default directory*/
	$db_object=db_connect();
	if(isset($_GET['lang'])&& isset($_GET['room'])){
		$language=$_GET['lang'];
		$room_id=$_GET['room'];
	}else{
		$language='el';
		$room_id=1;
	}
	$menu_titles=db_get_translated_room_menu_items($db_object,$language);
	$room_row_data = db_get_translated_room($db_object,$room_id,$language);
	$room_data=db_get_room($db_object,$room_id);	
	$room_slider_dir='../assets/img'.$room_data['slider_folder_path'];
	$room_gallery_dir='../assets/img'.$room_data['gallery_folder_path'];
	$editFormAction = $_SERVER['PHP_SELF'];
	if (isset($_SERVER['QUERY_STRING'])) {
		$editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
	}
	if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form2")) {
		db_update_translated_room($db_object,$room_id,$language,$_POST);
		$updateGoTo = "rooms.php?lang=$language&room=$room_id";
		header(sprintf("Location: %s", $updateGoTo));
	}
?>