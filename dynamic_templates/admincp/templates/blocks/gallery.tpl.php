﻿<div id="tab-container2" class='tab-container'>
 <ul class='etabs nav nav-tabs nav-justified'>
   <li class='tab active'><a data-toggle="tab" href="#tab1">Μεγάλες</a></li>
   <li class='tab'><a data-toggle="tab" href="#tab2">Μικρες</a></li>
 </ul>
 <div class='panel-container tab-content'>
  <div id="tab1" class="tab-pane fade active in">
<table width="750" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b6b6b">
  <div style="text-align:center;font-size:18px;font-weight:bold;padding:10px 0px;"> Gallery</br>
: <?php echo $gallery_dir; ?></br></div>
  <tr>
    <td><div align="center"><strong></strong></div></td>
    <td><div align="center"><strong></strong></div></td>
    <td><div align="center"><strong></strong></div></td>
  </tr>
  <?php foreach($gallery_images as $image) { ?>
  <tr>
   	<td><div align="center"><img src="<?php echo $gallery_dir; ?>/<?php echo $image; ?>" width="250" height="150"/></div></td>
    <td><div align="center"><?php echo $image; ?></div></td>
    <td><div align="center"><a href="inc.functions.php?delete=<?php echo $image; ?>&dir=<?php echo $gallery_dir; ?>&redirect=<?php echo $redirect; ?>"><img src="assets/img/icon-delete.png" alt="delete" border="0" /></a></div></td>
  </tr>
  <?php } ?>
  <form action="inc.functions.php" method="post" enctype="multipart/form-data">
	<tr>
		<td>Gallery:</td>
		<td><input name="addimage" type="file" size="65" /></td>
		<td><div align="center"><input class="btn btn-lg btn-primary" name="add" type="submit" value="<?php echo $update_label; ?>" /></div></td>
		</tr>
	<input name="dir" type="hidden" value="<?php echo $gallery_dir; ?>" />
	<input name="redirect" type="hidden" value="<?php echo $redirect; ?>" />
	<input name="type" type="hidden" value="gallery" />
  </form>
</table>
</div>
<div id="tab2" class="tab-pane fade">
<table width="750" border="1" align="center" cellpadding="4" cellspacing="0" bordercolor="#6b6b6b">
  <div style="text-align:center;font-size:18px;font-weight:bold;padding:10px 0px;">
Thumbnails </br> : <?php echo $gallery_dir.'/thumbs'; ?></br></div>
  <tr>
    <td><div align="center"><strong></strong></div></td>
    <td><div align="center"><strong></strong></div></td>
    <td><div align="center"><strong></strong></div></td>
  </tr>
  <?php foreach($gallery_thumb_images as $image) { ?>
  <tr>
   	<td><div align="center"><img src="<?php echo $gallery_dir.'/thumbs'; ?>/<?php echo $image; ?>" width="150" height="100"/></div></td>
    <td><div align="center"><?php echo $image; ?></div></td>
    <td><div align="center"><a href="inc.functions.php?delete=<?php echo $image; ?>&dir=<?php echo $gallery_dir.'/thumbs'; ?>&redirect=<?php echo $redirect; ?>"><img src="assets/img/icon-delete.png" alt="delete" border="0" /></a></div></td>
  </tr>
  <?php } ?>
  <form action="inc.functions.php" method="post" enctype="multipart/form-data">
	<tr>
		<td> Gallery:</td>
		<td><input name="addimage" type="file" size="65" /></td>
		<td><div align="center"><input class="btn btn-lg btn-primary" name="add" type="submit" value="<?php echo $update_label; ?>" /></div></td>
		</tr>
	<input name="dir" type="hidden" value="<?php echo $gallery_dir.'/thumbs'; ?>" />
	<input name="type" type="hidden" value="gallery" />
	<input name="redirect" type="hidden" value="<?php echo $redirect; ?>" />
  </form>
</table>
</div>
</div>