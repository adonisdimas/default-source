<div id="menu">
  <form id="form1" name="form1" method="get" action="">
    <table width="900" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="138"><?php echo $lang_select; ?>: </td>
        <td width="120"><label>
          <select name="lang" id="lang" onchange="check_and_submit();">
            <option value="0">------------</option>
			<option value="el"><?php echo $lang_el; ?></option>
            <option value="en"><?php echo $lang_en; ?></option>
            <option value="fr"><?php echo $lang_fr; ?></option>
          </select>
        </label></td>
        <td width="126"><?php echo $room_select; ?>: </td>
        <td width="126"><label>
          <select name="room" id="room" onchange="check_and_submit();">
            <option value="0" selected="selected">-------------</option>
            <?php 
            foreach ($menu_titles as $key => $menu_title){
            	$key = $key+1;
            	echo "<option value=$key>$menu_title</option>";	
            }
            ?>	     
          </select>
        </label></td>
        <td class="">
        	<div align="right"><a href="<?php echo ROOT; ?>settings.php"><?php echo $general_label; ?></a></div>
        	<div align="right"><a href="<?php echo ROOT; ?>pages.php"><?php echo $pages_label; ?></a></div>
        	<div align="right"><a href="<?php echo $logoutAction ?>"><?php echo $logout_label; ?></a></div>        
        </td>
      </tr>
    </table>
  </form>
</div>