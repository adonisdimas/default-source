<div id="tab-container" class='tab-container'>
 <ul class='etabs nav nav-tabs nav-justified'>
   	<li class='tab active'><a data-toggle="tab" href="#tabs1"><?php echo $admin_content; ?></a></li>
   	<li class='tab'><a data-toggle="tab" href="#tabs2"><?php echo $admin_slider; ?></a></li>
   	<li class='tab'><a data-toggle="tab" href="#tabs3"><?php echo $admin_gallery; ?></a></li>
 </ul>
 <div class='panel-container tab-content'>
  <div id="tabs1" class="tab-pane fade active in">
	<form action="<?php echo $editFormAction; ?>" method="post" name="form2" id="form2">
	<br />
	  <table width="900" align="center" cellpadding="0" cellspacing="5">
		<tr style="border-bottom: 1px solid #eee;padding:5px;margin:10px;display:inline-block;width:100%;">
		<?php if (isset($room_row_data['menu_title'])) { ?>
		<tr valign="baseline">
		  <td align="right" valign="top" nowrap="nowrap">
		  	<div align="left"><?php echo $content_menu_title; ?>:</div>
		 	<input type="text" name="menu_title" class="form-control" value="<?php echo $room_row_data['menu_title']; ?>" size="32" />
		  </td>
		</tr>
		<?php } ?>
		<tr style="border-bottom: 1px solid #eee;padding:5px;margin:10px;display:inline-block;width:100%;">
		<tr valign="baseline">
		  <td align="right" valign="top" nowrap="nowrap">
		  	<div align="left"><?php echo $content_title; ?>:</div>
		  	<input type="text" name="title" class="form-control" value="<?php echo $room_row_data['title']; ?>" size="32" />
		  </td>
		</tr>
		<tr style="border-bottom: 1px solid #eee;padding:5px;margin:10px;display:inline-block;width:100%;">
		<?php if (isset($room_row_data['content_1'])) { ?>
		<tr valign="baseline">
		  <td align="right" valign="top" nowrap="nowrap">
		  	<div align="left"><?php echo $content_1; ?>:</div>
		  	<textarea name="content_1" cols="70" rows="5" class=" ckeditor" ><?php echo $room_row_data['content_1']; ?></textarea>
		  </td>
		</tr>
		<?php } ?>
		<tr style="border-bottom: 1px solid #eee;padding:5px;margin:10px;display:inline-block;width:100%;">
		<?php if (isset($room_row_data['content_2'])) { ?>
		<tr valign="baseline">
		  <td align="right" valign="top" nowrap="nowrap">
		  	<div align="left"><?php echo $content_2; ?>:</div>
		  	<textarea name="content_2" cols="70" rows="5" class="ckeditor" ><?php echo $room_row_data['content_2']; ?></textarea>
		  </td>
		</tr>
		<?php } ?>
		<tr style="border-bottom: 1px solid #eee;padding:5px;margin:10px;display:inline-block;width:100%;">
		<?php if (isset($room_row_data['content_3'])) { ?>
		<tr valign="baseline">
		  <td align="right" valign="top" nowrap="nowrap">
		  	<div align="left"><?php echo $content_3; ?>:</div>
		  	<textarea name="content_3" cols="70" rows="5" class="ckeditor" ><?php echo $room_row_data['content_3']; ?></textarea>
		  </td>
		</tr>
		<?php } ?>
		<tr style="border-bottom: 1px solid #eee;padding:5px;margin:10px;display:inline-block;width:100%;">
		<tr  valign="baseline" id="submit-button">
			<td align="right" valign="top" nowrap="nowrap">
		  		<input type="submit" class="btn btn-lg btn-primary btn-block" value="<?php echo $update_label; ?>" />
		  	</td>
		</tr>
	  </table>
	  <input type="hidden" name="MM_update" value="form2" />
	  <input name="lang" type="hidden" id="lang" value="<?php echo $language; ?>" />
	  <input name="room" type="hidden" id="room" value="<?php echo $room_id; ?>" />
	</form>
  </div>
  <div id="tabs2" class="tab-pane fade">
 	 <?php
  			$script_name = basename($_SERVER['SCRIPT_NAME'], '.php');
			$slider_dir = $room_slider_dir;
			if(!is_dir_invalid($slider_dir)){
				$slider_images=GetImages($slider_dir);
			}
			$redirect = "rooms.php?lang=$language&room=$room_id#tabs2";
			include("templates/blocks/slider.tpl.php");
	 ?>
  </div>
  <div id="tabs3" class="tab-pane fade">
 	 <?php
 	 	$gallery_dir = $room_gallery_dir;
		if(!is_dir_empty($gallery_dir)){
			$gallery_images=GetImages($gallery_dir);
			$gallery_thumb_images=GetImages($gallery_dir.'/thumbs');
		}else{
			$gallery_images='';
		}
		$redirect = "rooms.php?lang=$language&room=$room_id#tabs3";
		include("templates/blocks/gallery.tpl.php");
?>
  </div>
	  </div>
	<p>&nbsp;</p>
 </div>
</div>