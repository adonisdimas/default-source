<!DOCTYPE html>
<!--[if lt IE 7]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if gt IE 8]><!-->
<html>
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Administration</title>
		<link href="<?php echo ROOT.ASSETS; ?>/css/bootstrap.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo ROOT.ASSETS; ?>/css/main.css" rel="stylesheet" type="text/css" />
		<script src="<?php echo ROOT.ASSETS; ?>/js/jquery.min.js"></script>
		<script src="<?php echo ROOT.ASSETS; ?>/js/bootstrap.min.js"></script>
		<script src="<?php echo ROOT.ASSETS; ?>/../ckeditor/ckeditor.js"></script>
		<script src="<?php echo ROOT.ASSETS; ?>/js/main.js"></script>