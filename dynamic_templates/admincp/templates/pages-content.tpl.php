<div id="tab-container" class='tab-container'>
 <ul class='etabs nav nav-tabs nav-justified'>
   <li class='tab active'><a data-toggle="tab" href="#tabs1"><?php echo $admin_content; ?></a></li>
    <?php if ($page_id != 5) {?>
	<li class='tab'><a data-toggle="tab" href="#tabs2"><?php echo $admin_slider; ?></a></li>
	<?php } ?>
   	<li class='tab'><a data-toggle="tab" href="#tabs3"><?php echo $admin_gallery; ?></a></li>
 </ul>
 <div class='panel-container tab-content'>
  <div id="tabs1" class="tab-pane fade active in">
	<form action="<?php echo $editFormAction; ?>" method="post" name="form2" id="form2">
	<br />
	  <table width="900" align="center" cellpadding="0" cellspacing="5">
		<?php if (isset($page_row_data['meta_title'])) { ?>
		<tr valign="baseline">
		<td width="900" height="30" align="right" valign="top" nowrap="nowrap">
			<div align="left"><?php echo $content_meta_title; ?>:</div>
			<input type="text" name="meta_title" class="form-control" value="<?php echo $page_row_data['meta_title']; ?>" size="32" />
		</td>
		<?php } ?>
		</tr>
		<tr style="border-bottom: 1px solid #eee;padding:5px;margin:10px;display:inline-block;width:100%;">
		<?php if (isset($page_row_data['meta_description'])) { ?>
		<tr valign="baseline">
		  <td align="right" valign="top" nowrap="nowrap">
		  	<div align="left"><?php echo $content_description; ?>:</div>
			<textarea name="meta_description" cols="50" rows="3" class="form-control"><?php echo $page_row_data['meta_description']; ?></textarea>
		  </td>
		  <?php } ?>
		</tr>
		<tr style="border-bottom: 1px solid #eee;padding:5px;margin:10px;display:inline-block;width:100%;">
		<?php if (isset($page_row_data['meta_keywords'])) { ?>
		<tr valign="baseline">
		  <td align="right" valign="top" nowrap="nowrap">
		  	<div align="left"><?php echo $content_keywords; ?>:</div>
		  	<textarea name="meta_keywords" cols="50" rows="4" class="form-control"><?php echo $page_row_data['meta_keywords']; ?></textarea>
		  </td>
		  <?php } ?>
		</tr>
		<tr style="border-bottom: 1px solid #eee;padding:5px;margin:10px;display:inline-block;width:100%;">
		<?php if (isset($page_row_data['menu_title'])) { ?>
		<tr valign="baseline">
		  <td align="right" valign="top" nowrap="nowrap">
		  	<div align="left"><?php echo $content_menu_title; ?>:</div>
		 	<input type="text" name="menu_title" class="form-control" value="<?php echo $page_row_data['menu_title']; ?>" size="32" />
		  </td>
		</tr>
		<?php } ?>
		<tr style="border-bottom: 1px solid #eee;padding:5px;margin:10px;display:inline-block;width:100%;">
		<tr valign="baseline">
		  <td align="right" valign="top" nowrap="nowrap">
		  	<div align="left"><?php echo $content_title; ?>:</div>
		  	<input type="text" name="title" class="form-control" value="<?php echo $page_row_data['title']; ?>" size="32" />
		  </td>
		</tr>
		<tr style="border-bottom: 1px solid #eee;padding:5px;margin:10px;display:inline-block;width:100%;">
		<?php if (isset($page_row_data['content_1'])) { ?>
		<tr valign="baseline">
		  <td align="right" valign="top" nowrap="nowrap">
		  	<div align="left"><?php echo $content_1; ?>:</div>
		  	<textarea name="content_1" cols="70" rows="5" class=" ckeditor" ><?php echo $page_row_data['content_1']; ?></textarea>
		  </td>
		</tr>
		<?php } ?>
		<tr style="border-bottom: 1px solid #eee;padding:5px;margin:10px;display:inline-block;width:100%;">
		<?php if (isset($page_row_data['content_2'])) { ?>
		<tr valign="baseline">
		  <td align="right" valign="top" nowrap="nowrap">
		  	<div align="left"><?php echo $content_2; ?>:</div>
		  	<textarea name="content_2" cols="70" rows="5" class="ckeditor" ><?php echo $page_row_data['content_2']; ?></textarea>
		  </td>
		</tr>
		<?php } ?>
		<tr style="border-bottom: 1px solid #eee;padding:5px;margin:10px;display:inline-block;width:100%;">
		<tr  valign="baseline" id="submit-button">
			<td align="right" valign="top" nowrap="nowrap">
		  		<input type="submit" class="btn btn-lg btn-primary btn-block" value="<?php echo $update_label; ?>" />
		  	</td>
		</tr>
	  </table>
	  <input type="hidden" name="MM_update" value="form2" />
	  <input name="lang" type="hidden" id="lang" value="<?php echo $language; ?>" />
	  <input name="page" type="hidden" id="page" value="<?php echo $page_id; ?>" />
	</form>
  </div>
  <div id="tabs2" class="tab-pane fade">
	<!--If we are in the gallery page, load the gallery admin editor else load the slider-->
	<?php
		if ($page_id != 5) {
			$script_name = basename($_SERVER['SCRIPT_NAME'], '.php');
			$slider_dir = $page_slider_dir;
			if(!is_dir_invalid($slider_dir)){
				$slider_images=GetImages($slider_dir);
			}else{
				$slider_images='';
			}
			$redirect = "pages.php?lang=$language&page=$page_id#tabs2";
			include("templates/blocks/slider.tpl.php");
		}
	?>
	</div>
  <div id="tabs3" class="tab-pane fade">
	<?php 
	    if ($page_id == 5) {
			$gallery_dir = GALLERY_DIR;
	    }else{
	    	$gallery_dir = $page_gallery_dir;
	    }
		if(!is_dir_empty($gallery_dir)){
			$gallery_images=GetImages($gallery_dir);
			$gallery_thumb_images=GetImages($gallery_dir.'/thumbs');
		}else{
			$gallery_images='';
		}
		$redirect = "pages.php?lang=$language&page=$page_id#tabs3";
		include("templates/blocks/gallery.tpl.php");
	?>
  </div>
	  </div>
	<p>&nbsp;</p>
 </div>
</div>