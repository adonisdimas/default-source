﻿<div id="tab-container" class='tab-container'>
 <ul class='etabs nav nav-tabs nav-justified'>
   <li class='tab active'><a data-toggle="tab" href="#tabs1"><?php echo $admin_page_teasers; ?></a></li>
   <li class='tab'><a data-toggle="tab" href="#tabs2"><?php echo $admin_room_teasers; ?></a></li>
   <li class='tab'><a data-toggle="tab" href="#tabs3"><?php echo $admin_social; ?></a></li>
   <li class='tab'><a data-toggle="tab" href="#tabs4"><?php echo $admin_strings; ?></a></li>
   <li class='tab'><a data-toggle="tab" href="#tabs5"><?php echo $admin_offer; ?></a></li>
 </ul>
 <div class='panel-container tab-content'>
	  <div id="tabs1" class="tab-pane fade active in">
    	<form action="<?php echo $editFormAction; ?>" method="post" name="form2" id="form2">
	  	  	<table width="900" align="center" cellpadding="0" cellspacing="5">
		    <?php if (isset($page_teasers_row_data)) { ?>
			    <?php foreach ($page_teasers_row_data as $key => $page_teaser){ ?>
		        	<tr valign="baseline">
						<td width="900" height="30" align="right" valign="top" nowrap="nowrap">
							<div align="left">link:</div>
							<input type="text" name="page_teaser[<?php echo $key; ?>][link]" class="form-control" value="<?php echo $page_teaser['link']; ?>" size="32" />
						</td>
					</tr>
					<tr valign="baseline">
						<td width="900" height="30" align="right" valign="top" nowrap="nowrap">
							<div align="left">Image Path:</div>
							<input type="text" name="page_teaser[<?php echo $key; ?>][image_path]" class="form-control" value="<?php echo $page_teaser['image_path']; ?>" size="32" />
						</td>
					</tr>
					<input type="hidden" name="page_teaser[<?php echo $key; ?>][id]" class="form-control" value="<?php echo $page_teaser['id']; ?>" size="32" />
					<?php $translated_page_teaser=db_get_translated_setting($db_object,$page_teaser['id'] ,$language);?>
					<tr valign="baseline">
						<td width="900" height="30" align="right" valign="top" nowrap="nowrap">
							<div align="left">Title:</div>
							<input type="text" name="page_teaser[<?php echo $key; ?>][title]" class="form-control" value="<?php echo $translated_page_teaser[0]['title']; ?>" size="32" />
						</td>
					</tr>
					<tr valign="baseline">
						<td width="900" height="30" align="right" valign="top" nowrap="nowrap">
							<div align="left">Content:</div>
							<textarea name="page_teaser[<?php echo $key; ?>][content_1]" cols="50" rows="3" class="ckeditor"><?php echo $translated_page_teaser[0]['content_1']; ?></textarea>
						</td>
					</tr>
					<tr style="border-bottom: 1px solid #eee;padding:5px;margin:10px;display:inline-block;width:100%;"></tr>
		        <?php }?>
	        <?php }?>
			<tr style="border-bottom: 1px solid #eee;padding:5px;margin:10px;display:inline-block;width:100%;">
			<tr  valign="baseline" id="submit-button">
				<td align="right" valign="top" nowrap="nowrap">
			  		<input type="submit" class="btn btn-lg btn-primary btn-block" value="<?php echo $update_label; ?>" />
			  	</td>
			</tr>
			<input type="hidden" name="MM_update" value="form2" />
			<input name="lang" type="hidden" id="lang" value="<?php echo $language; ?>" />
	       	</table>
       	</form>
	  </div>
	  <div id="tabs2" class="tab-pane fade">
    	<form action="<?php echo $editFormAction; ?>" method="post" name="form3" id="form3">
	  	  	<table width="900" align="center" cellpadding="0" cellspacing="5">
		    <?php if (isset($room_teasers_row_data)) { ?>
			    <?php foreach ($room_teasers_row_data as $key => $room_teaser){ ?>
		        	<tr valign="baseline">
						<td width="900" height="30" align="right" valign="top" nowrap="nowrap">
							<div align="left">link:</div>
							<input type="text" name="room_teaser[<?php echo $key; ?>][link]" class="form-control" value="<?php echo $room_teaser['link']; ?>" size="32" />
						</td>
					</tr>
					<tr valign="baseline">
						<td width="900" height="30" align="right" valign="top" nowrap="nowrap">
							<div align="left">Image Path:</div>
							<input type="text" name="room_teaser[<?php echo $key; ?>][image_path]" class="form-control" value="<?php echo $room_teaser['image_path']; ?>" size="32" />
						</td>
					</tr>
					<input type="hidden" name="room_teaser[<?php echo $key; ?>][id]" class="form-control" value="<?php echo $room_teaser['id']; ?>" size="32" />
					<?php $translated_room_teaser=db_get_translated_setting($db_object,$room_teaser['id'] ,$language);?>
					<tr valign="baseline">
						<td width="900" height="30" align="right" valign="top" nowrap="nowrap">
							<div align="left">Title:</div>
							<input type="text" name="room_teaser[<?php echo $key; ?>][title]" class="form-control" value="<?php echo $translated_room_teaser[0]['title']; ?>" size="32" />
						</td>
					</tr>
					<tr valign="baseline">
						<td width="900" height="30" align="right" valign="top" nowrap="nowrap">
							<div align="left">Content:</div>
							<textarea name="room_teaser[<?php echo $key; ?>][content_1]" cols="50" rows="3" class="ckeditor form-control"><?php echo $translated_room_teaser[0]['content_1']; ?></textarea>
						</td>
					</tr>
					<tr style="border-bottom: 1px solid #eee;padding:5px;margin:10px;display:inline-block;width:100%;"></tr>
		        <?php }?>
	        <?php }?>
			<tr style="border-bottom: 1px solid #eee;padding:5px;margin:10px;display:inline-block;width:100%;">
			<tr  valign="baseline" id="submit-button">
				<td align="right" valign="top" nowrap="nowrap">
			  		<input type="submit" class="btn btn-lg btn-primary btn-block" value="<?php echo $update_label; ?>" />
			  	</td>
			</tr>
			<input type="hidden" name="MM_update" value="form3" />
			<input name="lang" type="hidden" id="lang" value="<?php echo $language; ?>" />
	       	</table>
       	</form>
	  </div>
	  <div id="tabs3" class="tab-pane fade">
    	<form action="<?php echo $editFormAction; ?>" method="post" name="form4" id="form4">
	  	  	<table width="900" align="center" cellpadding="0" cellspacing="5">
		    <?php if (isset($social_row_data)) { ?>
			    <?php foreach ($social_row_data as $key => $social){ ?>
		        	<tr valign="baseline">
						<td width="900" height="30" align="right" valign="top" nowrap="nowrap">
							<div align="left">link:</div>
							<input type="text" name="social[<?php echo $key; ?>][link]" class="form-control" value="<?php echo $social['link']; ?>" size="32" />
						</td>
					</tr>
					<input type="hidden" name="social[<?php echo $key; ?>][image_path]" class="form-control" value='<?php echo $social['image_path']; ?>' size="32" />
					<input type="hidden" name="social[<?php echo $key; ?>][id]" class="form-control" value="<?php echo $social['id']; ?>" size="32" />
					<tr style="border-bottom: 1px solid #eee;padding:5px;margin:10px;display:inline-block;width:100%;"></tr>
		        <?php }?>
	        <?php }?>
			<tr style="border-bottom: 1px solid #eee;padding:5px;margin:10px;display:inline-block;width:100%;">
			<tr  valign="baseline" id="submit-button">
				<td align="right" valign="top" nowrap="nowrap">
			  		<input type="submit" class="btn btn-lg btn-primary btn-block" value="<?php echo $update_label; ?>" />
			  	</td>
			</tr>
			<input type="hidden" name="MM_update" value="form4" />
			<input name="lang" type="hidden" id="lang" value="<?php echo $language; ?>" />
	       	</table>
       	</form>
	  </div>
	  <div id="tabs4" class="tab-pane fade">
    	<form action="<?php echo $editFormAction; ?>" method="post" name="form5" id="form5">
	  	  	<table width="900" align="center" cellpadding="0" cellspacing="5">
		    <?php if (isset($strings_row_data)) { ?>
			    <?php foreach ($strings_row_data as $key => $strings){ ?>
		        	<tr valign="baseline">
						<td width="900" height="30" align="right" valign="top" nowrap="nowrap">
							<div align="left"><?php echo $strings['label']; ?></div>
							<textarea name="strings[<?php echo $key; ?>][value]" cols="50" rows="3" class="ckeditor form-control"><?php echo $strings['value']; ?></textarea>
							<input type="hidden" name="strings[<?php echo $key; ?>][id]" class="form-control" value="<?php echo $strings['id']; ?>" size="32" />
						</td>
					</tr>
					<tr style="border-bottom: 1px solid #eee;padding:5px;margin:10px;display:inline-block;width:100%;"></tr>
		        <?php }?>
	        <?php }?>
			<tr  valign="baseline" id="submit-button">
				<td align="right" valign="top" nowrap="nowrap">
			  		<input type="submit" class="btn btn-lg btn-primary btn-block" value="<?php echo $update_label; ?>" />
			  	</td>
			</tr>
			<input type="hidden" name="MM_update" value="form5" />
			<input name="lang" type="hidden" id="lang" value="<?php echo $language; ?>" />
	       	</table>
       	</form>
	  </div>
	  <div id="tabs5" class="tab-pane fade">
    	<form action="<?php echo $editFormAction; ?>" method="post" name="form6" id="form6">
	  	  	<table width="900" align="center" cellpadding="0" cellspacing="5">
		    <?php if (isset($offers_row_data)) { ?>
			    <?php foreach ($offers_row_data as $key => $offers){ ?>
		        	<tr valign="baseline">
						<td width="900" height="30" align="right" valign="top" nowrap="nowrap">
							<div align="left"><?php echo $admin_offer_check; ?></div>
							<input type="checkbox" name="offers[flag]" style="float:left;" <?php if ($offers['flag']==1)echo 'checked="checked"' ?> >
						</td>
					</tr>
					<tr style="border-bottom: 1px solid #eee;padding:5px;margin:10px;display:inline-block;width:100%;"></tr>
					<tr valign="baseline">
						<td width="900" height="30" align="right" valign="top" nowrap="nowrap">
							<div align="left"><?php echo $admin_offer_1; ?></div>
							<textarea name="offers[content]" cols="50" rows="3" style="float:left;" class="ckeditor"><?php echo $offers['content']; ?></textarea>
						</td>
					</tr>
					<input type="hidden" name="offers[id]" class="form-control" value="<?php echo $offers['id']; ?>" size="32" />
					<tr style="border-bottom: 1px solid #eee;padding:5px;margin:10px;display:inline-block;width:100%;"></tr>
		        <?php }?>
	        <?php }?>
			<tr  valign="baseline" id="submit-button">
				<td align="right" valign="top" nowrap="nowrap">
			  		<input type="submit" class="btn btn-lg btn-primary btn-block" value="<?php echo $update_label; ?>" />
			  	</td>
			</tr>
			<input type="hidden" name="MM_update" value="form6" />
	       	</table>
       	</form>
	  </div>
	<p>&nbsp;</p>
 </div>
</div>