<?php 
	include('includes/loader.php');
	require_once 'includes/session-logout.php';
	require_once 'includes/rooms-functions.php';
	include(TEMPLATES.'/head.tpl.php');
?>
<script type="text/javascript">
	function check_and_submit() {
		x = document.form1.lang.selectedIndex;
		y = document.form1.room.selectedIndex;
		if ((x != 0) && (y != 0)) {
			document.form1.action = "rooms.php?lang=" + document.form1.lang.options[x].value + "&room=" + (document.form1.room.options[y].value)
			document.form1.submit();
		}
	}
	function check_options(){
		lang = '<?php echo $language; ?>';
		room = <?php echo $room_id; ?>;
		switch(lang) {
			case 'el':
				document.form1.lang[1].selected = "1"
				break;
			case 'en':
				document.form1.lang[2].selected = "2"
				break;
			case 'fr':
				document.form1.lang[3].selected = "3"
				break;
		}
		document.form1.room[room].selected =  '<?php echo $room_id; ?>';
	}
</script>
</head>
<body onload="check_options();">
<header>
	<?php include(TEMPLATES.'/header.tpl.php');?>
</header>
<?php include(TEMPLATES.'/rooms-menu.tpl.php');?>
<?php include(TEMPLATES.'/rooms-content.tpl.php');?>
 <footer>
 	<?php include(TEMPLATES.'/footer.tpl.php');?>
 </footer>
</body>
</html>