<?php 
	include('includes/loader.php');
	require_once 'includes/session-logout.php';
	require_once 'includes/settings-functions.php';
	include(TEMPLATES.'/head.tpl.php');
?>
<script type="text/javascript">
	function check_and_submit() {
		x = document.form1.lang.selectedIndex;
		document.form1.action = "settings.php?lang=" + document.form1.lang.options[x].value;
		document.form1.submit();
	}
	function check_options(){
		lang = '<?php echo $language; ?>';
		switch(lang) {
			case 'el':
				document.form1.lang[1].selected = "1"
				break;
			case 'en':
				document.form1.lang[2].selected = "2"
				break;
			case 'fr':
				document.form1.lang[3].selected = "3"
				break;
		}
	}
</script>
</head>
<body onload="check_options();">
<header>
	<?php include(TEMPLATES.'/header.tpl.php');?>
</header>
 <?php include(TEMPLATES.'/settings-menu.tpl.php');?>
 <?php include(TEMPLATES.'/settings-content.tpl.php');?>
 <footer>
 	<?php include(TEMPLATES.'/footer.tpl.php');?>
 </footer>
</body>
</html>