<?php 
	include('includes/loader.php');
	require_once 'includes/session-logout.php';
	require_once 'includes/pages-functions.php';
	include(TEMPLATES.'/head.tpl.php');
?>
<script type="text/javascript">
	function check_and_submit() {
		x = document.form1.lang.selectedIndex;
		y = document.form1.page.selectedIndex;
		if ((x != 0) && (y != 0)) {
			document.form1.action = "pages.php?lang=" + document.form1.lang.options[x].value + "&page=" + (document.form1.page.options[y].value);
			document.form1.submit();
		}
	}
	function check_options(){
		lang = '<?php echo $language; ?>';
		page = '<?php echo $page_id; ?>';
		switch(lang) {
			case 'el':
				document.form1.lang[1].selected = "1"
				break;
			case 'en':
				document.form1.lang[2].selected = "2"
				break;
			case 'fr':
				document.form1.lang[3].selected = "3"
				break;
		}
		document.form1.page[page].selected =  '<?php echo $page_id; ?>';
	}
</script>
</head>
<body onload="check_options();">
<header>
	<?php include(TEMPLATES.'/header.tpl.php');?>
</header>
<?php include(TEMPLATES.'/pages-menu.tpl.php');?>
<?php include(TEMPLATES.'/pages-content.tpl.php');?>
 <footer>
 	<?php include(TEMPLATES.'/footer.tpl.php');?>
 </footer>
</body>
</html>