<?php include(TEMPLATES.'/head.tpl.php');?>
</head>
	<body>
		<header>
			<?php include(TEMPLATES.'/header.tpl.php');?>
		</header>
		<div class="wrapper">
		   <form class="form-main" action="<?php echo $loginFormAction; ?>" method="POST" name="form1" id="form1">       
		     <h2 class="form-main-heading"><?php echo $admin_label; ?></h2>
		     <input name="username" type="text" class="form-control" placeholder="Όνομα Χρήστη" id="username" required/>
		     <input name="password" type="password" class="form-control"  placeholder="Κωδικός" id="password" required/>      
		     <label class="checkbox">
		       <input type="checkbox" value="remember-me" id="rememberMe" name="rememberMe"><?php echo $remember_label; ?>
		     </label>
		     <input class="btn btn-lg btn-primary btn-block" type="submit" name="button" id="button" value="<?php echo $login_label; ?>" />
		   </form>
		 </div>
		 <footer>
		 	<?php include(TEMPLATES.'/footer.tpl.php');?>
		 </footer>
	</body>
</html>