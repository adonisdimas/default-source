/* Global settings */
var settings = {
	debug : false
};
/* Output to console if debug is on */
function echo( message ) {
	if ( settings.debug ) {
		console.log( message );
	};
};
/* IE detection flag */
jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})();
/* Global help-vars */
var resizeTimeout;
(function($){
	var app = {
		'init': function(e) {
		echo( 'App init...' );
		$(window).on('resize', app.onResize);
		},
		'onResize' : function() {
			clearTimeout( resizeTimeout );
			resizeTimeout = setTimeout(function() {
			}, 10);
		}

	};
	$(document).on('ready', app.init);
})(jQuery);

