<?php
require_once  '../includes/functions.php';
/*Adding an image and then redirect to specified page*/
if(isset($_POST['add']))  {
	/*Adding the image*/
 	$newfile = "$_POST[dir]/".$_FILES['addimage']['name'];
 	copy($_FILES['addimage']['tmp_name'], $newfile);
	/*Adding a copy of the new image to thumbs directory*/
	if($_POST['type']='gallery'){
		//createThumb($newfile,"$_POST[dir]/thumbs/".$_FILES['addimage']['name'], 400);
	}
	header("Location: $_POST[redirect]", true, 302);
}
/*Deleting an image and then redirect to specified page*/
if (isset($_GET['delete'])) {
	unlink("$_GET[dir]/$_GET[delete]");
	unlink("$_GET[dir]/thumbs/$_GET[delete]");
	if(isset($_GET['page'])){
		header("Location: $_GET[redirect]&page=$_GET[page]");
	}else{
		header("Location: $_GET[redirect]&room=$_GET[room]");
	}
}
/*Creating thumbnails for images in a directory*/
function createThumbs( $pathToImages, $pathToThumbs, $thumbWidth )
{
	// open the directory
	$dir = opendir( $pathToImages );
	while (false !== ($fname = readdir( $dir ))) {
		createThumb($pathToImages . $fname, $pathToThumbs . $fname, $thumbWidth);
	}
	// close the directory
	closedir( $dir );
}
function createThumb($src,$dest,$desired_width = false, $desired_height = false)
{
	/*If no dimenstion for thumbnail given, return false */
	if (!$desired_height&&!$desired_width) return false;

	$fparts = pathinfo($src);
	if(!$fparts['extension'])return false;
	$ext = strtolower($fparts['extension']);
	/* if its not an image return false */
	if (!in_array($ext,array('gif','jpg','png','jpeg'))) return false;

	/* read the source image */
	if ($ext == 'gif')
		$resource = imagecreatefromgif($src);
		else if ($ext == 'png')
			$resource = imagecreatefrompng($src);
			else if ($ext == 'jpg' || $ext == 'jpeg')
				$resource = imagecreatefromjpeg($src);
				$width  = imagesx($resource);
				$height = imagesy($resource);
				/* find the "desired height" or "desired width" of this thumbnail, relative to each other, if one of them is not given  */
				if(!$desired_height) $desired_height = floor($height*($desired_width/$width));
				if(!$desired_width)  $desired_width  = floor($width*($desired_height/$height));
				/* create a new, "virtual" image */
				$virtual_image = imagecreatetruecolor($desired_width,$desired_height);
				/* copy source image at a resized size */
				imagecopyresized($virtual_image,$resource,0,0,0,0,$desired_width,$desired_height,$width,$height);
				/* create the physical thumbnail image to its destination */
				/* Use correct function based on the desired image type from $dest thumbnail source */
				$fparts = pathinfo($dest);
				$ext = strtolower($fparts['extension']);
				/* if dest is not an image type, default to jpg */
				if (!in_array($ext,array('gif','jpg','png','jpeg'))) $ext = 'jpg';
				$dest = $fparts['dirname'].'/'.$fparts['filename'].'.'.$ext;
				echo $fparts['filename'];
				if ($ext == 'gif')
					imagegif($virtual_image,$dest);
					else if ($ext == 'png')
						imagepng($virtual_image,$dest,1);
						else if ($ext == 'jpg' || $ext == 'jpeg')
							imagejpeg($virtual_image,$dest,100);
							return array(
									'width'     => $width,
									'height'    => $height,
									'new_width' => $desired_width,
									'new_height'=> $desired_height,
									'dest'      => $dest
							);
}
?>
