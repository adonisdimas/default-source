<?php if (isset($offers_data)) { ?>
	<?php if ($offers_data[0]['flag']==1) { ?>
	  <div class="modal" tabindex="-1" role="dialog" id="offer-modal">
	    <div class="modal-dialog modal-lg" role="document">
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	          <h4 class="modal-title"><?php echo $modal_offer_title; ?></h4>
	        </div>
	        <div class="modal-body">
	          <p><?php echo $offers_data[0]['content']; ?></p>
	        </div>
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $modal_offer_close; ?></button>
	        </div>
	      </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	  </div><!-- /.modal -->
	<?php }?>
<?php }?>