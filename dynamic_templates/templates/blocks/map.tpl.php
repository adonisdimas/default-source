<div class="content-padding content-width content-padding">
	<div class="col-md-12"><h1 class="lines"><span class="decor-l"></span><?php echo $map_title; ?><span class="decor-r"></span></h1></div>		
</div>
<div class="col-md-12">
	<div class="map-wrapper">
		<div class="map">
			<div id="map-canvas"></div>
		</div>
	</div>
</div>
