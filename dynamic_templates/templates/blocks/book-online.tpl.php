<div class="book-online">
	<a href="" class="book-mobile"><?php echo $book_online_label ?><</a>
	<form class="book-online-form content-width content-padding form-inline" id="booking_form" name="booking_form" method="post" action="" target="new">
		<div class="form-group">
	      <input type="text" placeholder="<?php echo $book_online_form_label1 ?>" class="calender" id="datepicker1">
	      <i class="fa fa-calendar" aria-hidden="true"></i>
	    </div>
		<div class="form-group">
	      <input type="text" placeholder="<?php echo $book_online_form_label2 ?>" class="calender" id="datepicker2">
			<i class="fa fa-calendar" aria-hidden="true"></i>
	    </div>
	   	<div class="form-group">
			<select name="rooms" class="form-select" id="form-rooms">
				<option selected disabled><?php echo $book_online_form_label3 ?></option>
				<option value="1" class="rooms-option">1</option>
				<option value="2" class="rooms-option">2</option>
				<option value="3" class="rooms-option">3</option>
				<option value="4" class="rooms-option">4</option>
			</select>
			<i class="fa fa-key" aria-hidden="true"></i>
			<input id="rooms" type="hidden" name="rooms" value="1">
		</div>
		<div class="form-group">
			<select name="adults" class="form-select" id="form-adults">
				<option selected disabled><?php echo $book_online_form_label4 ?></option>
				<option value="1" class="adults-option">1</option>
				<option value="2" class="adults-option">2</option>
				<option value="3" class="adults-option">3</option>
				<option value="4" class="adults-option">4</option>
				<option value="5" class="adults-option">5</option>
			</select>
			<i class="fa fa-user" aria-hidden="true"></i>
			<input id="adults" type="hidden" name="adults" value="2">
		</div>
		<div class="form-group">
			<select name="children" class="form-select last" id="form-children">
				<option selected disabled><?php echo $book_online_form_label5 ?></option>
				<option value="1" class="children-option">1</option>
				<option value="2" class="children-option">2</option>
				<option value="3" class="children-option">3</option>
			</select>
			<i class="fa fa-male" aria-hidden="true"></i>
			<input id="children" type="hidden" name="children" value="0">
		</div>
		<input type="submit" value="<?php echo $book_online_label ?>" class="" onclick="if (ValidateDates11a('<?php echo $language ?>')){document.getElementById(&quot;booking_form&quot;).submit();}">
	</form>
</div>