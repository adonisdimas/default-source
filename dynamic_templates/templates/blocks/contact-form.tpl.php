<form class="contact-form content-width-small" method="post" action='<?php echo LOCALE_ROOT; ?>includes/mailer.php'>
    <div class="form-group row">
    	<div class="col-md-4">
    		<input type="text" name="firstname" placeholder = "<?php echo $contact_form_name; ?>" required="">
    		<input type="email" name="email" placeholder = "<?php echo $contact_form_email; ?>" required="">
    		<input type="text" name="sec_code" id="sec_code" placeholder = "<?php echo $contact_form_insert; ?>" class="textfield_2">
			<img class="sec_code"
				src="<?php echo LOCALE_ROOT; ?>includes/verificationimage.php?randstring=<?php echo rand(999,9999); ?>"
				alt="verification image, type it in the box" name="ver_img"
				align="top" id="ver_img"> <input
				type="text" name="sec_code" id="sec_code" class="textfield_2"
				disabled>
		    <?php if($_GET['wrong_code'] == "bad"){?>
		    <div style="background-color:#D70000; color:#FFFFFF; height:30px; padding:0px; padding-left:5px;width:185px;margin-left:0px;font-size:10px;font-size: 10px;position:absolute;top: 15px;right: 15px;">
		    <div align="center"><?php echo $contact_form_message_wrong_code; ?></div>
		    </div>
		    <?php ;}?>
		    <?php if($_GET['wrong_code'] == "good"){?>
		    <div style="background-color:#00d700; color:#FFFFFF; padding:0px; height:30px; padding-left:5px;width:185px;margin-left:0px;position: absolute;font-size: 10px;position:absolute;top: 15px;right: 15px;">
		    <div><?php echo $contact_form_message_success; ?></div>
		    </div>
		    <?php ;}?>    	
		</div>
	    <div class="col-md-8">
	    	<textarea name="message" rows="10" cols="30" placeholder = "<?php echo $contact_form_message; ?>" required=""></textarea>
	    </div>
    </div>
    <br>
    <input type="submit" value="<?php echo $contact_form_button; ?>">
</form>