<div class="row">
	<div class="col-md-12"><h1 class="lines"><span class="decor-l"></span><?php echo $rooms_teasers_title; ?><span class="decor-r"></span></h1></div>
	<div class="col-md-12 top-bottom-padding">
		<div class="row top-bottom-padding">
		    <?php for($i=0;$i<sizeof($room_teasers_data);$i++): ?>
		   		<?php $translated_room_teaser_data= db_get_translated_setting($db_object,$room_teasers_data[$i]['id'],$language);?>
				<div class="col-md-3 room <?php if($i==3){ echo 'last';}?>">
				    <div class="upper">
						<h2 class="underline"><?php echo $translated_room_teaser_data[0]['title'];?></h2>
						<?php echo $translated_room_teaser_data[0]['content_1'];?>
				    </div>
					<a href="<?php echo LOCALE_ROOT; ?><?php echo $room_teasers_data[$i]['link']; ?>"><img src="<?php echo ROOT.ASSETS.$room_teasers_data[$i]['image_path']; ?>" /></a>
					<div class="button"><a href="<?php echo LOCALE_ROOT; ?><?php echo $room_teasers_data[$i]['link']; ?>"><?php echo $button_title1; ?><i class="fa fa-chevron-right" aria-hidden="true"></i></a></div>
				</div>		   
		    <?php endfor; ?>
		</div>
	</div>
</div>