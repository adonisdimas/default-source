<div class="row highlights-teaser">
	<div class="col-md-12"><h1 class="lines"><span class="decor-l"></span><?php echo $pages_teasers_title; ?><span class="decor-r"></span></h1></div>
	<div class="col-md-12 top-bottom-padding">
		<div class="row">
		   	<?php $translated_page_teaser_data= db_get_translated_setting($db_object,$page_teasers_data[0]['id'],$language);?>
			<div class="col-md-8 teaser1">
				<img src="<?php echo ROOT.ASSETS.$page_teasers_data[0]['image_path']; ?>" />
				<div class="block"><?php echo $translated_page_teaser_data[0]['content_1'] ?></div>
			</div>
		   	<?php $translated_page_teaser_data= db_get_translated_setting($db_object,$page_teasers_data[1]['id'],$language);?>
			<div class="col-md-4 teaser2">
				<img src="<?php echo ROOT.ASSETS.$page_teasers_data[1]['image_path']; ?>" />
				<div class="block"><a href=""><?php echo $translated_page_teaser_data[0]['content_1'] ?></span> <i class="fa fa-chevron-right" aria-hidden="true"></i></a></div>
			</div>							
		</div>
	</div>
</div>