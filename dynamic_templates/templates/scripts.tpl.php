<script src="<?php echo ROOT.ASSETS; ?>/js/lib/jquery.min.js" defer></script>
<script src="<?php echo ROOT.ASSETS; ?>/js/lib/jquery-ui.min.js" defer></script>
<script src="<?php echo ROOT.ASSETS; ?>/js/lib/bootstrap.min.js" defer></script>
<script src="<?php echo ROOT.ASSETS; ?>/js/lib/jquery.fancybox.min.js" defer></script>
<script src="<?php echo ROOT.ASSETS; ?>/js/lib/fotorama.min.js" defer></script>
<script src="<?php echo ROOT.ASSETS; ?>/js/lib/bootstrap.min.js" defer></script>
<script src="<?php echo ROOT.ASSETS; ?>/js/lib/masonry.min.js" defer></script>
<script src="<?php echo ROOT.ASSETS; ?>/js/lib/owl.carousel.min.js" defer></script>
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="<?php echo ROOT.ASSETS; ?>/js/lib/bookonline.js" defer></script>
<script src="<?php echo ROOT.ASSETS; ?>/js/main.js" defer></script>
<!-- Compatibility scripts to use in older IE shitty versions -->
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
<![endif]-->