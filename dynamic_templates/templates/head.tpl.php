<!DOCTYPE html>
<!--[if lt IE 7]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8]>    <html lang="en" class="no-js ie8"> <![endif]-->
<html>
	<head>
	<meta charset="utf-8" />
	<title><?php echo $translated_page_data['meta_title'] ?></title>
	<meta content="IE=edge" http-equiv="X-UA-Compatible" />
	<meta name="description" content="<?php echo $translated_page_data['meta_description']; ?>" />
	<meta name="keywords" content="<?php echo $translated_page_data['meta_keywords']; ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
  	<link href="<?php echo ROOT.ASSETS; ?>/css/lib/bootstrap.min.css" rel="stylesheet" />
  	<link href="<?php echo ROOT.ASSETS; ?>/css/lib/jquery.fancybox.min.css" rel="stylesheet" >
  	<link href="<?php echo ROOT.ASSETS; ?>/css/lib/jquery-ui.min.css" rel="stylesheet" />
  	<link href="<?php echo ROOT.ASSETS; ?>/css/lib/animate.min.css" rel="stylesheet" />
  	<link href="<?php echo ROOT.ASSETS; ?>/css/lib/font-awesome.min.css" rel="stylesheet" />
   	<link href="<?php echo ROOT.ASSETS; ?>/css/lib/owl.carousel.min.css" rel="stylesheet" />
  	<link href="<?php echo ROOT.ASSETS; ?>/css/lib/fotorama.css" rel="stylesheet" />
	<link href="<?php echo ROOT.ASSETS; ?>/css/style.css" rel="stylesheet" />	
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700&subset=greek,greek-ext" rel="stylesheet">