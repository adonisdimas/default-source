<?php 
	include('../includes/loader.php');
	include('../'.TEMPLATES.'/head.tpl.php');
?>
</head>
<body>
	<header>
		<?php include('../'.TEMPLATES.'/header.tpl.php');?>
	</header>
	<main>
		<div class="row top-bottom-padding">
			<div class="content-padding content-width content-padding">
				<div class="row">
					<div class="col-md-12"><h1 class="lines"><span class="decor-l"></span><?php echo $translated_room_data['title']; ?><span class="decor-r"></span></h1></div>
					<?php echo $translated_room_data['content_1']; ?>
					<div class="col-md-12 center-align">
						<div class="row content-width-small">
							<?php echo $translated_room_data['content_2']; ?>
						</div>				
					</div>		
				</div>
			</div>
		</div>
		<div class="row top-padding">
			<div class="row">
				<?php include('../'.TEMPLATES.'/blocks/page-gallery.tpl.php');?>
			</div>
		</div>
	</main>
	<footer>
		<?php include('../'.TEMPLATES.'/footer.tpl.php');?>
	</footer>
	<?php include('../'.TEMPLATES.'/scripts.tpl.php');?>
</body>
</html>