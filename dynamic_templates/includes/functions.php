<?php
	/* get server protocol */
	function server_protocol(){
		if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
				return 'https';
		}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' ||
				!empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on') {
				return 'https';
		}
		return 'http';
	}
	function base(){
		return basename(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
	}
	/* debugger: print argument and then exit*/
	function debug($data){
		var_dump($data);
		exit;
	}
	/*redirect function*/
	function redirect($location){
		if (!headers_sent()) {
			header($location);
			exit;
		}
	}
	function get_pageid($name)
	{
		switch($name){
			case 'index':
				return 1;
				break;
			case 'location':
				return 2;
				break;
			case 'accommodation':
				return 3;
				break;
			case 'facilities':
				return 4;
				break;
			case 'photos':
				return 5;
				break;
			case 'contact':
				return 6;
				break;
			case 'restaurant':
				return 7;
				break;
			case 'offers':
				return 8;
				break;
			default:
				return 1;/*on a page not found, redirect to index page*/
		}
	}
	function get_roomid($name)
	{
		switch($name){
			case 'superior-room':
				return 1;
				break;
			case 'suite-pool-view':
				return 2;
				break;
			case 'deluxe-suite':
				return 3;
				break;
			case 'maisonette':
				return 4;
				break;
			default:
				return 1;
		}
	}
	/*Return an HMTL a list of images contained in the given relative path.
	 If no image detected or false directory, images from /images/gallery will be returned  */
	function get_gallery(){
		$gallery_list='<div id="gallery">';
		$gallery_dir=dirname(realpath(__file__)).'/../'.ASSETS.'/img/gallery';
		$gallery_thumbs_dir=dirname(realpath(__file__)).'/../'.ASSETS.'/img/gallery/thumbs';
		$files = new DirectoryIterator($gallery_dir);
		$files=SortFiles($files);
		$thumbs = new DirectoryIterator($gallery_thumbs_dir);
		$thumbs=SortFiles($thumbs);
		$j=1;
		$gallery_class='';
		for($i= 0;$i < count($files);$i++){
			/*Apply specific styling to gallery image thumbs based on the index*/
			if($j==1 or $j==4){
				$gallery_class='style1';
			}else if($j==2 or $j==3){
				$gallery_class='style2';
			}else if($j==5){
				$gallery_class='style3';
			}else{
				$gallery_class='style4';
			}
			$gallery_list .='<div class="photo-item '.$gallery_class.'"><a rel=group class="gallery-thumb" href="'.ROOT.ASSETS.'/img/gallery/'.$files[$i].'" data-lightbox="gallery" data-title=""><img src="'.ROOT.ASSETS.'/img/gallery/thumbs/'.$thumbs[$i].'" alt=""></a></div>';
			if($j==7){
				$j=0;
			}
			$j=$j+1;
		}
		$gallery_list .= '</div>';
		return $gallery_list;
	}
	/*Return an HMTL ul list of images contained in the given relative path. */
	function get_page_gallery($relativeFolderPath){
		$list='';
		$dir=dirname(realpath(__file__)).'/../'.ASSETS.'/img'.$relativeFolderPath;
		$thumbs_dir=dirname(realpath(__file__)).'/../'.ASSETS.'/img'.$relativeFolderPath.'/thumbs';
		if(!is_dir_empty($dir)){
			$list='<div class="page-gallery">';
			$files = new DirectoryIterator($dir);
			$files=SortFiles($files);
			$thumbs = new DirectoryIterator($thumbs_dir);
			$thumbs=SortFiles($thumbs);
			for($i= 0;$i < count($files);$i++){
				$list .='<a rel=group class="gallery-thumb" href="'.ROOT.ASSETS.'/img'.$relativeFolderPath.'/'.$files[$i].'" rel="carousel"><span></span><img src="'.ROOT.ASSETS.'/img'.$relativeFolderPath.'/thumbs/'.$thumbs[$i].'" alt=""></a>';
			}
			$list .= '</div>';
		}
		return $list;
	}
	/*Return an HMTL list of images contained in the given relative path else return void.*/
	function get_page_slider($relativeFolderPath){
		$list = '';
		$dir=dirname(realpath(__file__)).'/../'.ASSETS.'/img'.$relativeFolderPath;
		if(!is_dir_empty($dir)){
			$list='<div class="page-slider">';
			$files = new DirectoryIterator($dir);
			$files=SortFiles($files);
			$i=0;
			foreach ($files as $file) {
				$list .= '<img src="'.ROOT.ASSETS.'/img'.$relativeFolderPath.'/'.$file.'" border="0"/>';
				$i++;
			}
			$list .= '</div>';
		}
		return $list;
	}
	/*Return images of a given directory*/
	function GetImages($dir){
		$images = array();
		$files = new DirectoryIterator($dir);
		foreach ($files as $file) {
			if ($file->isFile()){
				array_unshift ($images, $files->getFilename());
			}
		}
		sort($images);
		return $images;
	}
	/*Check if directory is empty*/
	function is_dir_invalid($dir) {
		if (!is_readable($dir)) return TRUE;
		return false;
	}
	/*Check if directory is empty*/
	function is_dir_empty($dir) {
		if (!is_readable($dir)) return TRUE;
		$files = new DirectoryIterator($dir);
		foreach ($files as $file) {
			if ($file->isFile()){
				return FALSE;
			}
		}
		return TRUE;
	}
	function SortFiles($dir){
		$files = array();
		foreach ($dir as $file) {
			if ($file->isFile()){
				$files[] = $file->getFilename();
			}
		}
		sort($files);
		return $files;
	}
?>