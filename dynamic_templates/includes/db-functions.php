<?php
	/* Database functions*/
	function db_connect(){
		try {
		 	$db_object = new medoo([
				'database_type' => 'mysql',
				'database_name' => 'my_db',
				'server' => 'localhost',
				'username' => 'root',
				'password' => '',
				'charset' => 'utf8',
			]);
		}catch (Exception $e){
		       echo $e;
		}
		return $db_object;
	}
	function db_get_page($db_object,$id){
		 return $db_object->get("pages","*", ["id" => $id]);
	}
	function db_get_translated_page($db_object,$page_id,$lang){
		return $db_object->get("pages_translations","*",["AND" => [
			"page_id" => $page_id,
			"lang" => $lang]]
		);
	}
	function db_get_translated_menu_items($db_object,$lang){
		return $db_object->select("pages_translations","menu_title", ["lang" => $lang]);
	}
	function db_update_translated_page($db_object,$page_id,$lang,$params){
		return $db_object->update("pages_translations",[
				"menu_title" => $params['menu_title'],
				"meta_title" => $params['meta_title'],
				"meta_description" => $params['meta_description'],
				"meta_keywords" => $params['meta_keywords'],
				"title" => $params['title'],
				"content_1" => $params['content_1'],
				"content_2" => $params['content_2']],["AND" => [
				"page_id" => $page_id,
				"lang" => $lang]]
				);
	}
	function db_authenticate($db_object,$username,$password){
		return $db_object->select("users","*", ["AND" => [
				"username" => $username,
				"password" => $password]]);

	}
	function db_get_room($db_object,$id){
		return $db_object->get("rooms","*", ["id" => $id]);
	}
	function db_get_translated_room($db_object,$room_id,$lang){
		return $db_object->get("rooms_translations","*",["AND" => [
				"room_id" => $room_id,
				"lang" => $lang]]
				);
	}
	function db_get_translated_room_menu_items($db_object,$lang){
		return $db_object->select("rooms_translations","menu_title", ["lang" => $lang]);
	}
	function db_update_translated_room($db_object,$room_id,$lang,$params){
		return $db_object->update("rooms_translations",[
				"menu_title" => $params['menu_title'],
				"title" => $params['title'],
				"content_1" => $params['content_1'],
				"content_2" => $params['content_2'],"content_3" => $params['content_3']],["AND" => [
						"room_id" => $room_id,
						"lang" => $lang]]
				);
	}
	function db_get_settings($db_object,$type){
		return $db_object->select("settings","*", ["type" => $type]);
	}
	function db_get_translated_setting($db_object,$id,$lang){
		return $db_object->select("settings_translations","*",["AND" => [
				"setting_id" => $id,
				"lang" => $lang]]
				);
	}
	function db_update_setting($db_object,$setting_id,$link,$image_path){
		return $db_object->update("settings",[
				"link" => $link,
				"image_path" => $image_path],["AND" => [
						"id" => $setting_id]]
				);
	}
	function db_update_translated_setting($db_object,$lang,$setting_id,$title, $content_1){
		return $db_object->update("settings_translations",[
				"title" => $title,
				"content_1" => $content_1],["AND" => [
						"setting_id" => $setting_id,
						"lang" => $lang]]
				);
	}

	function db_get_translated_strings($db_object,$lang){
		return $db_object->select("strings","*", ["lang" => $lang]);
	}
	function db_update_translated_string($db_object,$id,$value){
		return $db_object->update("strings",[
				"value" => $value],["AND" => [
						"id" => $id]]
				);
	}
	function db_get_translated_offer($db_object,$lang){
		return $db_object->select("offers","*", ["lang" => $lang]);
	}
	function db_update_translated_offer($db_object,$params,$flag){
		return $db_object->update("offers",["content" => $params['content'],
				"flag" => $flag],["AND" => [
						"id" => $params['id']]]
				);
	}
	/**********************************DATABASE MIGRATIONS***************************************************/
	function db_migration($db_object){
		$db_object->query("-- -----------------USERS--------------------------------------------
		CREATE TABLE users
		(id INT AUTO_INCREMENT PRIMARY KEY,
		username VARCHAR(25),
		password VARCHAR(25)
		)DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci;
		-- ------------------------------------------------------------------------
		-- -----------------PAGES--------------------------------------------
		CREATE TABLE pages
		(id INT AUTO_INCREMENT PRIMARY KEY,
		name VARCHAR(255),
		slider_folder_path VARCHAR(255),
		gallery_folder_path VARCHAR(255)
		)DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci;
		-- ------------------------------------------------------------------------
		-- -----------------PAGES_TRANSLATIONS---------------------------
		CREATE TABLE pages_translations
		(id INT AUTO_INCREMENT PRIMARY KEY,
		page_id INT not null,
		lang VARCHAR(2),
		menu_title VARCHAR(255),
		meta_title VARCHAR(255),
		meta_description VARCHAR(255),
		meta_keywords VARCHAR(255),
		title VARCHAR(255),
		content_1 TEXT,
		content_2 TEXT,
		FOREIGN KEY (page_id) REFERENCES pages(id)
		)DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci;
		-- ------------------------------------------------------------------------
		-- -----------------ROOMS--------------------------------------------
		CREATE TABLE rooms
		(id INT AUTO_INCREMENT PRIMARY KEY,
		name VARCHAR(255),
		slider_folder_path VARCHAR(255),
		gallery_folder_path VARCHAR(255)
		)DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci;
		-- ------------------------------------------------------------------------
		-- -----------------ROOMS_TRANSLATIONS---------------------------
		CREATE TABLE rooms_translations
		(id INT AUTO_INCREMENT PRIMARY KEY,
		room_id INT not null,
		lang VARCHAR(2),
		menu_title VARCHAR(255),
		title VARCHAR(255),
		content_1 TEXT,
		content_2 TEXT,
		content_3 TEXT,
		FOREIGN KEY (room_id) REFERENCES rooms(id)
		)DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci;
		-- ------------------------------------------------------------------------
		-- -----------------SETTINGS--------------------------------------------
		CREATE TABLE settings
		(id INT AUTO_INCREMENT PRIMARY KEY,
	    type INT not null,
		link VARCHAR(255),
		image_path VARCHAR(255)
		)DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci;
		-- ------------------------------------------------------------------------
		-- -----------------SETTINGS_TRANSLATIONS---------------------------
		CREATE TABLE settings_translations
		(id INT AUTO_INCREMENT PRIMARY KEY,
		setting_id INT not null,
		lang VARCHAR(2),
		title VARCHAR(255),
		content_1 TEXT,
		FOREIGN KEY (setting_id) REFERENCES settings(id)
		)DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci;
		-- ------------------------------------------------------------------------
		-- -----------------STRINGS_TRANSLATIONS---------------------------
		CREATE TABLE strings
		(id INT AUTO_INCREMENT PRIMARY KEY,
		lang VARCHAR(2),
		label VARCHAR(255),
		value TEXT
		)DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci;
		-- ------------------------------------------------------------------------
		-- -----------------OFFERS---------------------------
		CREATE TABLE offers
		(id INT AUTO_INCREMENT PRIMARY KEY,
		lang VARCHAR(2),
		flag INT,
		content TEXT
		)DEFAULT CHARSET=utf8 DEFAULT COLLATE utf8_unicode_ci;
		-- ------------------------------------------------------------------------");
	}
	/*************************************************************************************/
	function db_default_insertions($db_object){
		$db_object->insert('users', [
				"id" => 1,
				"username" => 'admin',
				"password" =>'1234'
		]);
		$db_object->insert('pages', [
				"id" => 1,
				"name" => 'index',
				"slider_folder_path" =>"/index/slider",
				"gallery_folder_path" =>"/index/gallery"
		]);
		$db_object->insert('pages', [
				"id" => 2,
				"name" => 'location',
				"slider_folder_path" =>"/location/slider",
				"gallery_folder_path" =>"/location/gallery"
		]);
		$db_object->insert('pages', [
				"id" => 3,
				"name" => 'accommodation',
				"slider_folder_path" =>"/accommodation/slider",
				"gallery_folder_path" =>"/accommodation/gallery"
		]);
		$db_object->insert('pages', [
				"id" => 4,
				"name" => 'facilities',
				"slider_folder_path" =>"/facilities/slider",
				"gallery_folder_path" =>"/facilities/gallery"
		]);
		$db_object->insert('pages', [
				"id" => 5,
				"name" => 'photos',
				"slider_folder_path" =>"/photos/slider",
				"gallery_folder_path" =>"/photos/gallery"
		]);
		$db_object->insert('pages', [
				"id" => 6,
				"name" => 'contact',
				"slider_folder_path" =>"/contact/slider",
				"gallery_folder_path" =>"/contact/gallery"
		]);
		$db_object->insert('pages', [
				"id" => 7,
				"name" => 'restaurant',
				"slider_folder_path" =>"/restaurant/slider",
				"gallery_folder_path" =>"/restaurant/gallery"
		]);
		$db_object->insert('pages', [
				"id" => 8,
				"name" => 'offers',
				"slider_folder_path" =>"/offers/slider",
				"gallery_folder_path" =>"/offers/gallery"
		]);
		$db_object->insert('pages', [
				"id" => 9,
				"name" => 'about',
				"slider_folder_path" =>"/about/slider",
				"gallery_folder_path" =>"/about/gallery"
		]);
		$db_object->insert('pages', [
				"id" => 10,
				"name" => 'sample',
				"slider_folder_path" =>"/sample/slider",
				"gallery_folder_path" =>"/sample/gallery"
		]);
		/****************************************************************/
		$db_object->insert('pages_translations', [
				"id" => 1,
				"page_id" => 1,
				"lang" => 'en',
				"menu_title" => '',
				"meta_title" => '',
				"meta_description" => '',
				"meta_keywords" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('pages_translations', [
				"id" => 2,
				"page_id" => 1,
				"lang" => 'el',
				"menu_title" => '',
				"meta_title" => '',
				"meta_description" => '',
				"meta_keywords" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('pages_translations', [
				"id" => 3,
				"page_id" => 1,
				"lang" => 'fr',
				"menu_title" => '',
				"meta_title" => '',
				"meta_description" => '',
				"meta_keywords" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('pages_translations', [
				"id" => 4,
				"page_id" => 2,
				"lang" => 'en',
				"menu_title" => '',
				"meta_title" => '',
				"meta_description" => '',
				"meta_keywords" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('pages_translations', [
				"id" => 5,
				"page_id" => 2,
				"lang" => 'el',
				"menu_title" => '',
				"meta_title" => '',
				"meta_description" => '',
				"meta_keywords" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('pages_translations', [
				"id" => 6,
				"page_id" => 2,
				"lang" => 'fr',
				"menu_title" => '',
				"meta_title" => '',
				"meta_description" => '',
				"meta_keywords" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);		
		$db_object->insert('pages_translations', [
				"id" => 7,
				"page_id" => 3,
				"lang" => 'en',
				"menu_title" => '',
				"meta_title" => '',
				"meta_description" => '',
				"meta_keywords" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('pages_translations', [
				"id" => 8,
				"page_id" => 3,
				"lang" => 'el',
				"menu_title" => '',
				"meta_title" => '',
				"meta_description" => '',
				"meta_keywords" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('pages_translations', [
				"id" => 9,
				"page_id" => 3,
				"lang" => 'fr',
				"menu_title" => '',
				"meta_title" => '',
				"meta_description" => '',
				"meta_keywords" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);		
		$db_object->insert('pages_translations', [
				"id" => 10,
				"page_id" => 4,
				"lang" => 'en',
				"menu_title" => '',
				"meta_title" => '',
				"meta_description" => '',
				"meta_keywords" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('pages_translations', [
				"id" => 11,
				"page_id" => 4,
				"lang" => 'el',
				"menu_title" => '',
				"meta_title" => '',
				"meta_description" => '',
				"meta_keywords" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('pages_translations', [
				"id" => 12,
				"page_id" => 4,
				"lang" => 'fr',
				"menu_title" => '',
				"meta_title" => '',
				"meta_description" => '',
				"meta_keywords" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);		
		$db_object->insert('pages_translations', [
				"id" => 13,
				"page_id" => 5,
				"lang" => 'en',
				"menu_title" => '',
				"meta_title" => '',
				"meta_description" => '',
				"meta_keywords" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('pages_translations', [
				"id" => 14,
				"page_id" => 5,
				"lang" => 'el',
				"menu_title" => '',
				"meta_title" => '',
				"meta_description" => '',
				"meta_keywords" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('pages_translations', [
				"id" => 15,
				"page_id" => 5,
				"lang" => 'fr',
				"menu_title" => '',
				"meta_title" => '',
				"meta_description" => '',
				"meta_keywords" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);		
		$db_object->insert('pages_translations', [
				"id" => 16,
				"page_id" => 6,
				"lang" => 'en',
				"menu_title" => '',
				"meta_title" => '',
				"meta_description" => '',
				"meta_keywords" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('pages_translations', [
				"id" => 17,
				"page_id" => 6,
				"lang" => 'el',
				"menu_title" => '',
				"meta_title" => '',
				"meta_description" => '',
				"meta_keywords" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('pages_translations', [
				"id" => 18,
				"page_id" => 6,
				"lang" => 'fr',
				"menu_title" => '',
				"meta_title" => '',
				"meta_description" => '',
				"meta_keywords" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);		
		$db_object->insert('pages_translations', [
				"id" => 19,
				"page_id" => 7,
				"lang" => 'en',
				"menu_title" => '',
				"meta_title" => '',
				"meta_description" => '',
				"meta_keywords" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('pages_translations', [
				"id" => 20,
				"page_id" => 7,
				"lang" => 'el',
				"menu_title" => '',
				"meta_title" => '',
				"meta_description" => '',
				"meta_keywords" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('pages_translations', [
				"id" => 21,
				"page_id" => 7,
				"lang" => 'fr',
				"menu_title" => '',
				"meta_title" => '',
				"meta_description" => '',
				"meta_keywords" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);		
		$db_object->insert('pages_translations', [
				"id" => 22,
				"page_id" => 8,
				"lang" => 'en',
				"menu_title" => '',
				"meta_title" => '',
				"meta_description" => '',
				"meta_keywords" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('pages_translations', [
				"id" => 23,
				"page_id" => 8,
				"lang" => 'el',
				"menu_title" => '',
				"meta_title" => '',
				"meta_description" => '',
				"meta_keywords" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('pages_translations', [
				"id" => 24,
				"page_id" => 8,
				"lang" => 'fr',
				"menu_title" => '',
				"meta_title" => '',
				"meta_description" => '',
				"meta_keywords" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);	
		$db_object->insert('pages_translations', [
				"id" => 25,
				"page_id" => 9,
				"lang" => 'en',
				"menu_title" => '',
				"meta_title" => '',
				"meta_description" => '',
				"meta_keywords" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('pages_translations', [
				"id" => 26,
				"page_id" => 9,
				"lang" => 'el',
				"menu_title" => '',
				"meta_title" => '',
				"meta_description" => '',
				"meta_keywords" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('pages_translations', [
				"id" => 27,
				"page_id" => 9,
				"lang" => 'fr',
				"menu_title" => '',
				"meta_title" => '',
				"meta_description" => '',
				"meta_keywords" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('pages_translations', [
				"id" => 28,
				"page_id" => 10,
				"lang" => 'en',
				"menu_title" => '',
				"meta_title" => '',
				"meta_description" => '',
				"meta_keywords" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('pages_translations', [
				"id" => 29,
				"page_id" => 10,
				"lang" => 'el',
				"menu_title" => '',
				"meta_title" => '',
				"meta_description" => '',
				"meta_keywords" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('pages_translations', [
				"id" => 30,
				"page_id" => 10,
				"lang" => 'fr',
				"menu_title" => '',
				"meta_title" => '',
				"meta_description" => '',
				"meta_keywords" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('rooms', [
				"id" => 1,
				"name" => 'superior-room',
				"slider_folder_path" =>"/rooms/superior-room/slider",
				"gallery_folder_path" =>"/rooms/superior-room/gallery"
		]);
		$db_object->insert('rooms', [
				"id" => 2,
				"name" => 'suite-pool-view',
				"slider_folder_path" =>"/rooms/suite-pool-view/slider",
				"gallery_folder_path" =>"/rooms/suite-pool-view/gallery"
		]);
		$db_object->insert('rooms', [
				"id" => 3,
				"name" => 'deluxe-suite',
				"slider_folder_path" =>"/rooms/deluxe-suite/slider",
				"gallery_folder_path" =>"/rooms/deluxe-suite/gallery"
		]);
		$db_object->insert('rooms', [
				"id" => 4,
				"name" => 'maisonette',
				"slider_folder_path" =>"/rooms/maisonette/slider",
				"gallery_folder_path" =>"/rooms/maisonette/gallery"
		]);			
		$db_object->insert('rooms_translations', [
				"id" => 1,
				"room_id" => 1,
				"lang" => 'en',
				"menu_title" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('rooms_translations', [
				"id" => 2,
				"room_id" => 1,
				"lang" => 'el',
				"menu_title" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('rooms_translations', [
				"id" => 3,
				"room_id" => 1,
				"lang" => 'fr',
				"menu_title" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('rooms_translations', [
				"id" => 4,
				"room_id" => 2,
				"lang" => 'en',
				"menu_title" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('rooms_translations', [
				"id" => 5,
				"room_id" => 2,
				"lang" => 'el',
				"menu_title" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('rooms_translations', [
				"id" => 6,
				"room_id" => 2,
				"lang" => 'fr',
				"menu_title" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('rooms_translations', [
				"id" => 7,
				"room_id" => 3,
				"lang" => 'en',
				"menu_title" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('rooms_translations', [
				"id" => 8,
				"room_id" => 3,
				"lang" => 'el',
				"menu_title" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('rooms_translations', [
				"id" => 9,
				"room_id" => 3,
				"lang" => 'fr',
				"menu_title" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);		
		$db_object->insert('rooms_translations', [
				"id" => 10,
				"room_id" => 4,
				"lang" => 'en',
				"menu_title" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('rooms_translations', [
				"id" => 11,
				"room_id" => 4,
				"lang" => 'el',
				"menu_title" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);
		$db_object->insert('rooms_translations', [
				"id" => 12,
				"room_id" => 4,
				"lang" => 'fr',
				"menu_title" => '',
				"title" => '',
				"content_1" => '',
				"content_2" => ''
		]);		
		/**PAGE TEASERS type=1**/
		$db_object->insert('settings', [
				"id" => 1,
				"type" => 1,
				"link" =>'',
				"image_path" =>''
		]);
		$db_object->insert('settings', [
				"id" => 2,
				"type" => 1,
				"link" =>'',
				"image_path" =>''
		]);
		$db_object->insert('settings', [
				"id" => 3,
				"type" => 1,
				"link" =>'',
				"image_path" =>''
		]);
		$db_object->insert('settings_translations', [
				"id" => 1,
				"setting_id" => 1,
				"lang" => 'en',
				"title" => '',
				"content_1" => '',
		]);
		$db_object->insert('settings_translations', [
				"id" => 2,
				"setting_id" => 1,
				"lang" => 'el',
				"title" => '',
				"content_1" => '',
		]);
		$db_object->insert('settings_translations', [
				"id" => 3,
				"setting_id" => 1,
				"lang" => 'fr',
				"title" => '',
				"content_1" => '',
		]);
		$db_object->insert('settings_translations', [
				"id" => 4,
				"setting_id" => 2,
				"lang" => 'en',
				"title" => '',
				"content_1" => '',
		]);
		$db_object->insert('settings_translations', [
				"id" => 5,
				"setting_id" => 2,
				"lang" => 'el',
				"title" => '',
				"content_1" => '',
		]);
		$db_object->insert('settings_translations', [
				"id" => 6,
				"setting_id" => 2,
				"lang" => 'fr',
				"title" => '',
				"content_1" => '',
		]);		
		$db_object->insert('settings_translations', [
				"id" => 7,
				"setting_id" => 3,
				"lang" => 'en',
				"title" => '',
				"content_1" => '',
		]);
		$db_object->insert('settings_translations', [
				"id" => 8,
				"setting_id" => 3,
				"lang" => 'el',
				"title" => '',
				"content_1" => '',
		]);
		$db_object->insert('settings_translations', [
				"id" => 9,
				"setting_id" => 3,
				"lang" => 'fr',
				"title" => '',
				"content_1" => '',
		]);		
		/**ROOM TEASERS type=2**/
		$db_object->insert('settings', [
				"id" => 4,
				"type" => 2,
				"link" =>'',
				"image_path" =>''
		]);
		$db_object->insert('settings', [
				"id" => 5,
				"type" => 2,
				"link" =>'',
				"image_path" =>''
		]);
		$db_object->insert('settings', [
				"id" => 6,
				"type" => 2,
				"link" =>'',
				"image_path" =>''
		]);
		$db_object->insert('settings', [
				"id" => 7,
				"type" => 2,
				"link" =>'',
				"image_path" =>''
		]);
		$db_object->insert('settings_translations', [
				"id" => 10,
				"setting_id" => 4,
				"lang" => 'en',
				"title" => '',
				"content_1" => '',
		]);
		$db_object->insert('settings_translations', [
				"id" => 11,
				"setting_id" => 4,
				"lang" => 'el',
				"title" => '',
				"content_1" => '',
		]);
		$db_object->insert('settings_translations', [
				"id" => 12,
				"setting_id" => 4,
				"lang" => 'fr',
				"title" => '',
				"content_1" => '',
		]);
		$db_object->insert('settings_translations', [
				"id" => 13,
				"setting_id" => 5,
				"lang" => 'el',
				"title" => '',
				"content_1" => '',
		]);
		$db_object->insert('settings_translations', [
				"id" => 14,
				"setting_id" => 5,
				"lang" => 'en',
				"title" => '',
				"content_1" => '',
		]);
		$db_object->insert('settings_translations', [
				"id" => 15,
				"setting_id" => 5,
				"lang" => 'fr',
				"title" => '',
				"content_1" => '',
		]);
		$db_object->insert('settings_translations', [
				"id" => 16,
				"setting_id" => 6,
				"lang" => 'el',
				"title" => '',
				"content_1" => '',
		]);
		$db_object->insert('settings_translations', [
				"id" => 17,
				"setting_id" => 6,
				"lang" => 'en',
				"title" => '',
				"content_1" => '',
		]);
		$db_object->insert('settings_translations', [
				"id" => 18,
				"setting_id" => 6,
				"lang" => 'fr',
				"title" => '',
				"content_1" => '',
		]);
		$db_object->insert('settings_translations', [
				"id" => 19,
				"setting_id" => 7,
				"lang" => 'el',
				"title" => '',
				"content_1" => '',
		]);
		$db_object->insert('settings_translations', [
				"id" => 20,
				"setting_id" => 7,
				"lang" => 'en',
				"title" => '',
				"content_1" => '',
		]);
		$db_object->insert('settings_translations', [
				"id" => 21,
				"setting_id" => 7,
				"lang" => 'fr',
				"title" => '',
				"content_1" => '',
		]);				
		/**SOCIAL LINKS type=3**/
		$db_object->insert('settings', [
				"id" => 8,
				"type" => 3,
				"link" =>'',
				"image_path" =>''
		]);
		$db_object->insert('settings', [
				"id" => 9,
				"type" => 3,
				"link" =>'',
				"image_path" =>''
		]);
		$db_object->insert('settings', [
				"id" => 10,
				"type" => 3,
				"link" =>'',
				"image_path" =>''
		]);
		$db_object->insert('settings', [
				"id" => 11,
				"type" => 3,
				"link" =>'',
				"image_path" =>''
		]);
		/****************************************************************/
		$db_object->insert('strings', [
				"id" => 1,
				"label" => 'copyright',
				"value" => 'All rights reserved',
				"lang" =>'en'
		]);
		$db_object->insert('strings', [
				"id" => 2,
				"label" => 'copyright',
				"value" => 'All rights reserved',
				"lang" =>'el'
		]);
		$db_object->insert('strings', [
				"id" => 3,
				"label" => 'copyright',
				"value" => 'All rights reserved',
				"lang" =>'fr'
		]);
		$db_object->insert('strings', [
				"id" => 4,
				"label" => 'contact_details',
				"value" => '',
				"lang" =>'en'
		]);
		$db_object->insert('strings', [
				"id" => 5,
				"label" => 'contact_details',
				"value" => '',
				"lang" =>'el'
		]);
		$db_object->insert('strings', [
				"id" => 6,
				"label" => 'contact_details',
				"value" => '',
				"lang" =>'fr'
		]);		
		$db_object->insert('strings', [
				"id" => 7,
				"label" => 'Facilities',
				"value" => '',
				"lang" =>'en'
		]);
		$db_object->insert('strings', [
				"id" => 8,
				"label" => 'Facilities',
				"value" => '',
				"lang" =>'el'
		]);
		$db_object->insert('strings', [
				"id" => 9,
				"label" => 'Facilities',
				"value" => '',
				"lang" =>'fr'
		]);			
		/****************************************************************/
		$db_object->insert('offers', [
				"id" => 1,
				"content" => '',
				"flag" => 0,
				"lang" =>'en'
		]);
		$db_object->insert('offers', [
				"id" => 2,
				"content" => '',
				"flag" => 0,
				"lang" =>'el'
		]);
		$db_object->insert('offers', [
				"id" => 3,
				"content" => '',
				"flag" => 0,
				"lang" =>'fr'
		]);
	}
?>