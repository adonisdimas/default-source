<?php
// load the variables form address bar
$subject = "Mail from site ";
$message = $_POST["message"];
$from = $_POST["email"];
$verif_box = $_POST["sec_code"];
$name = $_POST["firstname"];

// remove the backslashes that normally appears when entering " or '
$message = stripslashes($message);
$subject = stripslashes($subject);
$from = stripslashes($from);
$name = stripslashes($name);

if(empty($name) || empty($from)){
	header('Location:../contact.php?wrong_code=empty');
	exit;
} else if (md5($verif_box).'a4xn46$^' == $_COOKIE['site']){
	// if verification code was correct send the message and show this page
	mail("info@site.com", $subject, "Name:".$name."\n\nMessage: ".$message, 'Content-type: text/plain; charset=utf-8'."\r\n"."From: $from"."\r\n");
	header("Location: ../contact.php?wrong_code=good");
	// delete the cookie so it cannot sent again by refreshing this page
	setcookie('site','');
} else {
	header("Location: ../contact.php?wrong_code=bad");
	exit;
}
?>