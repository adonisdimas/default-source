<?php
	ob_start();
	session_start();
	// Turn off error reporting
	error_reporting(1);
	require_once  'medoo.php';
	require_once  'functions.php';
	require_once  'db-functions.php';
	/*if lang parameter is set get it or else use default*/
	if(isset($_GET["lang"])){
		$language=$_GET["lang"];
	}else{
		$language= 'en';
	}
	if($language== 'en'){
		require_once('locale/en.php');
	}else if($language== 'el'){
		require_once('locale/el.php');
	}else if($language== 'fr'){
		require_once('locale/fr.php');
	}
	$url = $_SERVER['REQUEST_URI']; //returns the current URL
	$parts = explode('/',$url);
	define('ROOT', "http://$_SERVER[HTTP_HOST]/");  /*ROOT*/
	define('LOCALE_ROOT', ROOT.$language."/");  /*ROOT*/
	define('DEFAULT_LOCALE', $language);
	define('ASSETS', 'assets');
	define('INCLUDES', 'includes');
	define('TEMPLATES', 'templates');
	$db_object=db_connect();
	db_migration($db_object);
	db_default_insertions($db_object);
	$page_subfolder = '';
	/*If we have a subfolder, load the rooms*/
	if(isset($parts[2])){
		$page_subfolder=$parts[1].'/';
		$page_name = basename($page_subfolder, '.php');
		/*Load Room Data from database*/
		$room_name = basename($_SERVER['SCRIPT_NAME'], '.php');
		$room_id = get_roomid($room_name);
		$room_data=db_get_room($db_object,$room_id);
		$translated_room_data = db_get_translated_room($db_object,$room_id,$language);
	}else{
		$page_name = basename($_SERVER['SCRIPT_NAME'], '.php');
		$room_name = $page_name;// for use in the header menu
	}
	/*Remove get parameters if any*/
	$page_name = strtok($page_name, '?');
	$page_name = basename($page_name, '.php');
	/*Load Page Data from database*/
	$page_id = get_pageid($page_name);
	$page_data=db_get_page($db_object,$page_id);
	$translated_page_data = db_get_translated_page($db_object,$page_id,$language);
	$translated_pages_menu_items = db_get_translated_menu_items($db_object,$language);
	/*Load Settings Data from database*/
	$page_teasers_data=db_get_settings($db_object,1);
	$room_teasers_data=db_get_settings($db_object,2);
	$social_data=db_get_settings($db_object,3);
	$strings_data=db_get_translated_strings($db_object,$language);
	$offers_data=db_get_translated_offer($db_object,$language);
	/***********************************************************************************/
?>