﻿<?php
/*******************TITLES************************/
$rooms_teasers_title = 'Standard rooms & Apartment';
$social_title ='Connect with us';
$contact_title ='Contact Details';
$map_title='Our Area Map';
$created_by="Created by";
$block_title1 ='Distances';
$block_title2 ='How to reach here';
$block_title3 ='CHECK AVAILABILITY';
$block_title4 ='ROOM GALLERY';
$button_title1 ='MORE INFORMATION';
$button_title2 ='read more >';
/*******************************************/
$teaser_title1 = 'Lorem ipsum';
$teaser_title2 = 'Lorem ipsum';
$teaser_title3 = 'Lorem ipsum';
$teaser_title4 = 'Lorem ipsum';
$teaser_title5 = 'Lorem ipsum';
$teaser_title6 = 'Lorem ipsum';
$teaser_read_more = 'read more';
$teaser_view = 'View Details';
/*******************MODAL OFFER************************/
$modal_offer_title = 'OFFER';
$modal_offer_close = 'CLOSE';
/*******************BOOK ONLINE************************/
$book_online_label = 'ONLINE BOOKING';
$book_online_form_label1='Check in';
$book_online_form_label2='Check out';
$book_online_form_label3='Rooms';
$book_online_form_label4='Adults';
$book_online_form_label5='Kids';
$book_online_button_label='CHECK RATES & <span>BOOK ONLINE</span>';
/*******************CONTACT FORM************************/
$contact_form_name ='Your Name';
$contact_form_email ='Your EMAIL';
$contact_form_insert = 'Insert Code';
$contact_form_message = 'Your Message';
$contact_form_message_wrong_code ='Wrong security code';
$contact_form_message_success ='Your message was sent successfully';
$contact_form_button = 'SEND';
/*******************DISTANCES************************/
$distance1= '<p>Μοναστήρι</p><span>20 Χλμ</span>';
$distance2= '<p>Χώρα</p><span>17 Χλμ</span>';
$distance3= '<p>Αιγιαλή</p><span>5 Χλμ</span>';
$distance4= '<p>Ορμός Αιγιαλής</p><span>2 Χλμ</span>';
$distance5= '<p>Κατάπολα</p><span>23 Χλμ</span>';
$distance6= '<p>Κατω μεριά</p><span>31 Χλμ</span>';
?>