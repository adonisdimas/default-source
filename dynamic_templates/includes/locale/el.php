﻿<?php
/*******************TITLES************************/
$rooms_teasers_title = 'Δωμάτια & Διαμερίσμα';
$social_title ='Επικοινωνήστε μαζί μας';
$contact_title ='Στοιχεία Επικοινωνίας';
$map_title='Ο Χάρτης μας';
$created_by="Δημιουργήθηκε από";
$block_title1 ='Αποστάσεις';
$block_title2 ='Πώς να φτάσετε εδώ';
$block_title3 ='CHECK AVAILABILITY';
$block_title4 ='ΦΩΤΟΓΡΑΦΙΕΣ ΔΩΜΑΤΙΩΝ';
$button_title1 ='ΠΕΡΙΣΣΟΤΕΡΕΣ ΠΛΗΡΟΦΟΡΙΕΣ';
$button_title2 ='Διαβάστε Περισσότερα >';
/*******************************************/
$teaser_title1 = 'Lorem ipsum';
$teaser_title2 = 'Lorem ipsum';
$teaser_title3 = 'Lorem ipsum';
$teaser_title4 = 'Lorem ipsum';
$teaser_title5 = 'Lorem ipsum';
$teaser_title6 = 'Lorem ipsum';
$teaser_read_more = 'read more';
$teaser_view = 'View Details';
/*******************MODAL OFFER************************/
$modal_offer_title = 'ΟΙ ΠΡΟΣΦΟΡΕΣ ΜΑΣ';
$modal_offer_close = 'ΚΛΕΙΣΙΜΟ';
/*******************BOOK ONLINE************************/
$book_online_label = 'ONLINE BOOKING';
$book_online_form_label1='Άφιξη';
$book_online_form_label2='Αναχώρηση';
$book_online_form_label3='Δωμάτια';
$book_online_form_label4='Ενήλικες';
$book_online_form_label5='Παιδιά';
$book_online_button_label='CHECK RATES & <span>BOOK ONLINE</span>';
/*******************CONTACT FORM************************/
$contact_form_name ='Το όνομα σας';
$contact_form_email ='Το email σας';
$contact_form_insert = 'Εισάγετε τον κωδικό';
$contact_form_message = 'Το μήνυμα σας';
$contact_form_message_wrong_code ='Λάθος κωδικός ασφαλείας';
$contact_form_message_success ='Το μήνυμα σας εστάλει επιτυχώς';
$contact_form_button = 'ΑΠΟΣΤΟΛΗ';
/*******************DISTANCES************************/
$distance1= '<p>Μοναστήρι</p><span>20 χλμ</span>';
$distance2= '<p>Χώρα</p><span>17 χλμ</span>';
$distance3= '<p>Αιγιαλή</p><span>5 χλμ</span>';
$distance4= '<p>Ορμός Αιγιαλής</p><span>2 χλμ</span>';
$distance5= '<p>Κατάπολα</p><span>23 χλμ</span>';
$distance6= '<p>Κατω μεριά</p><span>31 χλμ</span>';
?>